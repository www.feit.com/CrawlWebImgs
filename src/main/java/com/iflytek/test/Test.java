package com.iflytek.test;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {
	private static StringBuilder content = new StringBuilder();

	public static void main(String[] args) throws Exception {
		URL url = Test.class.getResource("/jsonview");
		readFile(url.getFile());
		String[] imgs = readImgUrl(content.toString());
		for (String img : imgs) {
			write(img);
		}
	}

	public static void readFile(String filePath) throws Exception {
		File rootFile = new File(filePath);
		File[] files = rootFile.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				readFile(file.getPath());
			} else {
				BufferedReader reader = new BufferedReader(new FileReader(file));
				System.out.println(file.getName());
				while (reader.read() != -1) {
					content.append(reader.readLine());
				}
				reader.close();
			}
		}
	}

	public static String[] readImgUrl(String content) throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		Pattern p = Pattern.compile("url[(][^)^(]*[)]");
		Matcher m = p.matcher(content);
		while (m.find()) {
			String path = m.group(0).replaceAll("'", "").replaceAll("url[(]", "").replaceAll("[)]", "").trim();
			if (path.startsWith("http")) {
				map.put(path, path);
			}
		}
		Pattern p2 = Pattern.compile("[']http://www[.]bejson[.]com/static/bejson/jsonview/ico1/[^']*[']");
		Matcher m2 = p2.matcher(content);
		while (m2.find()) {
			String path = m2.group(0).replaceAll("'", "").trim();
			if (path.startsWith("http")) {
				map.put(path, path);
			}
		}
		String[] paths = map.keySet().toArray(new String[map.keySet().size()]);
		System.err.println(paths.length);
		return paths;
	}

	public static void write(String destUrl) {
		System.out.println(destUrl);
		FileOutputStream fos = null;
		BufferedInputStream bis = null;
		HttpURLConnection httpUrl = null;
		URL url = null;
		int BUFFER_SIZE = 1024;
		byte[] buf = new byte[BUFFER_SIZE];
		int size = 0;
		try {
			String fileName = destUrl.substring(destUrl.lastIndexOf("/") + 1, destUrl.length());
			url = new URL(destUrl);
			httpUrl = (HttpURLConnection) url.openConnection();
			httpUrl.connect();
			bis = new BufferedInputStream(httpUrl.getInputStream());
			fos = new FileOutputStream(
					"E:\\\\zhanggang\\workspace_linyi\\Test\\src\\main\\resources\\out\\" + fileName);
			while ((size = bis.read(buf)) != -1) {
				fos.write(buf, 0, size);
			}
			fos.flush();
			fos.close();
			bis.close();
			httpUrl.disconnect();
		} catch (Exception e) {
			System.err.println(destUrl);
			e.printStackTrace();
		}
	}

}
